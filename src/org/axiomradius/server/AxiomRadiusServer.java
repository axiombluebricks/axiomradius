///**
// * $Id: TestServer.java,v 1.6 2006/02/17 18:14:54 wuttke Exp $
// * Created on 08.04.2005
// * @author Matthias Wuttke
// * @version $Revision: 1.6 $
// */
package org.axiomradius.server;

import java.io.File;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;

import org.axiomradius.packet.AccessRequest;
import org.axiomradius.packet.RadiusPacket;
import org.axiomradius.util.RadiusException;
import org.axiomradius.util.RadiusServer;

import org.axiomradius.config.AxiomConfig;
import org.axiomradius.attribute.RadiusAttribute;

/**
 * Test server which terminates after 30 s. Knows only the client "localhost"
 * with secret "testing123" and the user "mw" with the password "test".
 */
public class AxiomRadiusServer extends RadiusServer {

    //private static Log logger = LogFactory.getLog(AxiomRadiusServer.class);
    private Map<String, String> _userList = new HashMap<String, String>();
    private static AxiomConfig _config = new AxiomConfig();
    
    public static String interval;
    

    public static void pauze() {
        while (!isContinue()) {
           //keep the duration small, so the game will be resumed shortly after
            // the user presses resume
            try {
                Thread.sleep(1000 * 60);
            } catch (Throwable e) {
                System.out.println("Exception in pauze: " + e.getMessage());
            }
        }
    }

    public static boolean isContinue() {
        boolean returnVal = false;
            // this method checks in the rest of your code, whether or not your user pressed resume
        //returnVal = ....
        return returnVal;
    }

    public static void main(String[] args)
            throws Throwable {
        RadiusServer server = new AxiomRadiusServer();
        File directory = new File(".");
        //logger.info("Looking for file AxiomRadius.ini in current directory:" + directory.getAbsolutePath());
        System.out.println("Looking for axiomradius.conf in directory:" + directory.getAbsolutePath() + ".....");
        try {
        //            Wini ini = new Wini(new File("AxiomRadius.ini"));
        //            Ini.Section section = ini.get("CONFIG");
        //            String gpId = section.get("GroupID");
        //            String ip = section.get("AxiomServerIP");
        //            String port = section.get("AxiomServerPort");
        //

            File f = new File("axiomradius.conf");
            _config.InitBy(f);
//            System.out.println("");
            
            server.setListenAddress(InetAddress.getByName(_config.radiusServer.getAuthIp()));
            //server.setListenAddress(InetAddress.getByName("192.168.0.50"));
            server.setAuthPort(_config.radiusServer.getAuthPort());
            server.setAcctPort(_config.radiusServer.getAccountPort());
            interval=_config.radiusServer.getStatusTimeinterval();
            server.start(_config.radiusServer.isAuthEnabled(), _config.radiusServer.isAccountEnabled());
            Thread.sleep(1000); //sleep for one seconds to let server start.
            if (!server.closing) {
                //logger.info("Server started.");
                System.out.println("Server started...");
                pauze();
            }
            System.out.println("Server stopped..Check the log file admin.log for more details.");
            server.stop();
        } catch (Throwable ex) {
            ex.printStackTrace();
            System.out.println("Exception: " + ex.getMessage() + ex.getCause().toString());
            //logger.info("Exception " + ex.getMessage() + ex.getCause().toString());
        }
    }

    
    
    @Override
    public RadiusPacket accessRequestReceived(AccessRequest accessRequest, InetSocketAddress client)
            throws RadiusException {
        System.out.println("Received Access-Request:" + accessRequest + "from client " + client.getAddress().getHostAddress());
        RadiusPacket packet = null;
        switch (_config.getAuthType(client.getAddress().getHostAddress())) {
            case 0:
                System.out.println("Got 'NOOTP' request with User " + accessRequest.getUserName() + ", client: " + client.getAddress().getHostAddress());
                packet = HandleNoOTP(accessRequest);
                break;
            case 1:
                System.out.println("Got 'ONLYOTP' request with User " + accessRequest.getUserName() + ", client: " + client.getAddress().getHostAddress());
                packet = HandleOTP(accessRequest);
                break;
            case 2:
                System.out.println("Got 'OTPPASSWORD' request with User " + accessRequest.getUserName() + ", client: " + client.getAddress().getHostAddress());
                packet = HandleOTPPassword(accessRequest);
                break;
            case 3:
                System.out.println("Got 'OTPCHALLENGE' request with User " + accessRequest.getUserName() + ", client: " + client.getAddress().getHostAddress());
                packet = HandleOTPChallenge(accessRequest);
                break;
            default:
                System.out.println("Got 'Invalid' request with User " + accessRequest.getUserName() + ", client: " + client.getAddress().getHostAddress());
                packet = new RadiusPacket(RadiusPacket.ACCESS_REJECT, accessRequest.getPacketIdentifier());
                packet.addAttribute("Reply-Message", "Invalid client type");

        }
        return packet;
    }

    RadiusPacket HandleNoOTP(AccessRequest accessRequest) {
        boolean retValue = false;
        RadiusPacket answer = null;
//        if (_config.radiusServer.isLdapValidate()) {
//            retValue = _config.ldapSetting.IsAuthenticated(accessRequest.getUserName(), accessRequest.getUserPassword());
//        }
            retValue = _config.ldapSetting.IsAuthenticated(accessRequest.getUserName(), accessRequest.getUserPassword());
        
        if (retValue) {
            answer = new RadiusPacket(RadiusPacket.ACCESS_ACCEPT, accessRequest.getPacketIdentifier());
            answer.addAttribute("Reply-Message", "Logging successfull, group List returned, fetch 'class' attribute");

        } else {
            System.out.println("Ldap Login failed, account:" + accessRequest.getUserName());
            answer = new RadiusPacket(RadiusPacket.ACCESS_REJECT, accessRequest.getPacketIdentifier());
            answer.addAttribute("Reply-Message", "Invalid username or password");
        }
        copyProxyState(answer, accessRequest);
        return answer;
    }

    RadiusPacket HandleOTP(AccessRequest accessRequest) {
        boolean retValue = false;
        RadiusPacket answer = null;
        retValue = _config.otpSetting.validateOTP(accessRequest.getUserName(), accessRequest.getUserPassword());
        if (retValue) {
            answer = new RadiusPacket(RadiusPacket.ACCESS_ACCEPT, accessRequest.getPacketIdentifier());
            answer.addAttribute("Reply-Message", "login successfull");
        } else {
            System.out.println("OTP Login failed, account:" + accessRequest.getUserName());
            answer = new RadiusPacket(RadiusPacket.ACCESS_REJECT, accessRequest.getPacketIdentifier());
            answer.addAttribute("Reply-Message", "Invalid otp");
        }
        copyProxyState(answer, accessRequest);
        return answer;
    }

    RadiusPacket HandleOTPPassword(AccessRequest accessRequest) {
        RadiusPacket answer = null;
        String otppassword = accessRequest.getUserPassword();
//        int index = accessRequest.getUserPassword().indexOf('.');
//        if (index != -1) {
            //String otp = accessRequest.getUserPassword().substring(0, index);
            //String passwrd = accessRequest.getUserPassword().substring(index + 1, accessRequest.getUserPassword().length());
            boolean ret = _config.otpSetting.validateOTPPassword(accessRequest.getUserName(), otppassword);
            if (ret == true) {
                //boolean retLdap = _config.ldapSetting.IsAuthenticated(accessRequest.getUserName(), passwrd);
                //if (retLdap) {
                    answer = new RadiusPacket(RadiusPacket.ACCESS_ACCEPT, accessRequest.getPacketIdentifier());
                    answer.addAttribute("Reply-Message", "Logging successfull, group List returned, fetch 'class' attribute");
//                } else {
//                    logger.debug("OTPPassword Failed for the account:" + accessRequest.getUserName() + "Invalid userName or Password");
//                    answer = new RadiusPacket(RadiusPacket.ACCESS_REJECT, accessRequest.getPacketIdentifier());
//                    answer.addAttribute("Reply-Message", "Invalid UserName or Password");
//                }
            } else {
                System.out.println("OTPPassword Failed for the account:" + accessRequest.getUserName() + "Invalid OTP");
                answer = new RadiusPacket(RadiusPacket.ACCESS_REJECT, accessRequest.getPacketIdentifier());
                answer.addAttribute("Reply-Message", "Invalid OTP");
            }
//        } else {
//            logger.debug("OTPPassword Failed for the account:" + accessRequest.getUserName() + ".Format should be otp.password");
//            answer = new RadiusPacket(RadiusPacket.ACCESS_REJECT, accessRequest.getPacketIdentifier());
//            answer.addAttribute("Reply-Message", "Invalid password format, it should be otp.password");
//        }
        copyProxyState(answer, accessRequest);
        return answer;
    }

    RadiusPacket HandleOTPChallenge(AccessRequest accessRequest) {
        RadiusPacket answer = null;
        String password = null;
        boolean callLdap = true;
        boolean resOtp = false;
        
        boolean retval = checkIfOTP(accessRequest);        
        if (retval == true) {
            System.out.println("Validating the otp for the user " + accessRequest.getUserName());
            password = _userList.get(accessRequest.getUserName());
            resOtp = _config.otpSetting.validateOTP(accessRequest.getUserName(), accessRequest.getUserPassword());
            if (resOtp == false) {
                callLdap = false;
            }
            _userList.remove(accessRequest.getUserName());
        } else {
            password = accessRequest.getUserPassword();
        }
        
        System.out.println("Validating the LDAP for the user " + accessRequest.getUserName());

        if (callLdap) {
            boolean retLdap = _config.ldapSetting.IsAuthenticated(accessRequest.getUserName(), password);
            if (retLdap && resOtp) {
                answer = new RadiusPacket(RadiusPacket.ACCESS_ACCEPT, accessRequest.getPacketIdentifier());
                answer.addAttribute("Reply-Message", "Logging successfull, group List returned, fetch 'class' attribute");
            } else if (!resOtp) {
                answer = new RadiusPacket(RadiusPacket.ACCESS_CHALLENGE, accessRequest.getPacketIdentifier());
                RadiusAttribute attrib = new RadiusAttribute();
                attrib.setAttributeType(24); //STATE;
                byte d[] = new byte[1];
                d[0] = 1;
                attrib.setAttributeData(d);
                answer.addAttribute(attrib);
                answer.addAttribute("Reply-Message", "Access-Challenge, send otp");
                if (!_userList.containsKey(accessRequest.getUserName())) {
                    _userList.put(accessRequest.getUserName(), password);
                }
            } else if (!retLdap) {
                System.out.println("OTPPassword Failed for the account:" + accessRequest.getUserName() + "Invalid userName or Password");
                answer = new RadiusPacket(RadiusPacket.ACCESS_REJECT, accessRequest.getPacketIdentifier());
                answer.addAttribute("Reply-Message", "Invalid UserName or Password");
            }
        } else {
            System.out.println("OTPPassword Failed for the account:" + accessRequest.getUserName() + "Invalid OTP");
            answer = new RadiusPacket(RadiusPacket.ACCESS_REJECT, accessRequest.getPacketIdentifier());
            answer.addAttribute("Reply-Message", "Invalid OTP");
        }

        copyProxyState(answer, accessRequest);
        return answer;
    }

    /*
    //original code 
    RadiusPacket HandleOTPChallenge(AccessRequest accessRequest) {
        RadiusPacket answer = null;
        String password = null;
        boolean callLdap = true;
        boolean resOtp = false;
        
        boolean retval = checkIfOTP(accessRequest);        
        if (retval) {
            System.out.println("Validating the otp for the user " + accessRequest.getUserName());
            password = _userList.get(accessRequest.getUserName());
            resOtp = _config.otpSetting.validateOTP(accessRequest.getUserName(), accessRequest.getUserPassword());
            if (resOtp == false) {
                callLdap = false;
            }
            _userList.remove(accessRequest.getUserName());
        } else {
            password = accessRequest.getUserPassword();
        }
        System.out.println("Validating the LDAP for the user " + accessRequest.getUserName());

        if (callLdap) {
            boolean retLdap = _config.ldapSetting.IsAuthenticated(accessRequest.getUserName(), password);
            if (retLdap && resOtp) {
                answer = new RadiusPacket(RadiusPacket.ACCESS_ACCEPT, accessRequest.getPacketIdentifier());
                answer.addAttribute("Reply-Message", "Logging successfull, group List returned, fetch 'class' attribute");
            } else if (!resOtp) {
                answer = new RadiusPacket(RadiusPacket.ACCESS_CHALLENGE, accessRequest.getPacketIdentifier());
                RadiusAttribute attrib = new RadiusAttribute();
                attrib.setAttributeType(24); //STATE;
                byte d[] = new byte[1];
                d[0] = 1;
                attrib.setAttributeData(d);
                answer.addAttribute(attrib);
                answer.addAttribute("Reply-Message", "Access-Challenge, send otp");
                if (!_userList.containsKey(accessRequest.getUserName())) {
                    _userList.put(accessRequest.getUserName(), password);
                }
            } else if (!retLdap) {
                System.out.println("OTPPassword Failed for the account:" + accessRequest.getUserName() + "Invalid userName or Password");
                answer = new RadiusPacket(RadiusPacket.ACCESS_REJECT, accessRequest.getPacketIdentifier());
                answer.addAttribute("Reply-Message", "Invalid UserName or Password");
            }
        } else {
            System.out.println("OTPPassword Failed for the account:" + accessRequest.getUserName() + "Invalid OTP");
            answer = new RadiusPacket(RadiusPacket.ACCESS_REJECT, accessRequest.getPacketIdentifier());
            answer.addAttribute("Reply-Message", "Invalid OTP");
        }

        copyProxyState(answer, accessRequest);
        return answer;
    }

    */
    
    boolean checkIfOTP(AccessRequest accessRequest) {
        RadiusAttribute attrib = accessRequest.getAttribute(24);
        if (attrib != null) {
            byte[] stateVal = attrib.getAttributeData();
            if (stateVal[0] == 1) //set to 0X01
            {
                System.out.println("Got OTPCHALLENGE , otp found.");
                return true;
            }
        }
        return false;
    }

    public String getUserPassword(String userName) {
        if (userName.equals("mw")) {
            return "test";
        } else {
            return null;
        }
    }

    public String getSharedSecret(InetSocketAddress client) {
        String key = _config.getClientSecret(client.getAddress().getHostAddress());
        return key;
    }
    
     public boolean isAuthEnabled(){
            return true;}
        
        public boolean isAccountEnabled(){
            return true;}
        
        public boolean isLdapValidateEnabled(){
        return true;
    }

}
