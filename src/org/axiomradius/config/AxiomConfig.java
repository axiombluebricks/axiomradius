package org.axiomradius.config;

import com.mollatech.axiom.radius.AxiomRadiusClient;
import com.mollatech.axiom.radius.AxiomRadiusConfiguration;
import com.mollatech.axiom.radius.AxiomRadiusInterfaceImplService;
import com.mollatech.axiom.radius.Axiomradius;
import java.io.File;
import java.io.Serializable;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.xml.namespace.QName;
import org.apache.log4j.Logger;
import static org.axiomradius.config.LoadSettings.LoadSettingsWithFile;
import org.axiomradius.util.AllTrustManager;
import org.axiomradius.util.AllVerifier;

public class AxiomConfig implements Serializable {

    private static Logger logger = Logger.getLogger("AxiomRadius");

    //private static Log logger = LogFactory.getLog(AxiomConfig.class);
    private Map<String, AxiomNasSetting> _nasSettings = null;
    //public RadiusServer radiusServer = null;
    public AxiomRadiusConfiguration radiusServer = null;
    public AxiomLdapSetting ldapSetting;
    public AxiomOTPSetting otpSetting;

    public enum authType {

        NOOTP, ONLYOTP, OTPPASSWORD, OTPCHALLENGE
    };

    private static AxiomRadiusInterfaceImplService m_gCoreService = null;
    private static Axiomradius m_gCorePort = null;
    private static String m_gChannelID = null;
    private static String m_gLoginID = null;
    private static String m_gLoginPassword = null;

    private static AxiomRadiusConfiguration getConfigrationSettings() {
        //if (m_gCoreService == null) {
        String wsdlname = LoadSettings.g_sSettings.getProperty("axiomradius.wsdl.name");
        m_gChannelID = LoadSettings.g_sSettings.getProperty("axiomradius.channelid");
        m_gLoginID = LoadSettings.g_sSettings.getProperty("axiomradius.remotelogin");
        m_gLoginPassword = LoadSettings.g_sSettings.getProperty("axiomradius.password");
        String ipAddress = LoadSettings.g_sSettings.getProperty("axiomradius.ipaddress");
        String strPort = LoadSettings.g_sSettings.getProperty("axiomradius.port");
        String strSecured = LoadSettings.g_sSettings.getProperty("axiomradius.secured");
        String log = LoadSettings.g_sSettings.getProperty("log.debug.enable");
        if (strSecured != null && strSecured.compareToIgnoreCase("yes") == 0) {
            SSLContext sslContext = null;
            try {
                HttpsURLConnection.setDefaultHostnameVerifier(new AllVerifier());
                try {
                    sslContext = SSLContext.getInstance("TLS");
                } catch (NoSuchAlgorithmException ex) {
                    //Logger.getLogger(NewClass.class.getName()).log(Level.SEVERE, null, ex);
                    ex.printStackTrace();
                }
                sslContext.init(null, new TrustManager[]{new AllTrustManager()}, null);
                HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

            } catch (KeyManagementException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        try { // Call Web Service Operation

            String wsdlUrl = "http://" + ipAddress + ":" + strPort + "/" + wsdlname + "/AxiomRadiusInterfaceImpl?wsdl";
            if (strSecured != null && strSecured.compareToIgnoreCase("yes") == 0) {
                wsdlUrl = "https://" + ipAddress + ":" + strPort + "/" + wsdlname + "/AxiomRadiusInterfaceImpl?wsdl";
            }

            URL url = new URL(wsdlUrl);
            QName qName = new QName("http://radius.axiom.mollatech.com/", "AxiomRadiusInterfaceImplService");

            m_gCoreService = new AxiomRadiusInterfaceImplService(url, qName);
            m_gCorePort = m_gCoreService.getAxiomRadiusInterfaceImplPort();
            return m_gCorePort.getConfigrationSettings(m_gChannelID, m_gLoginID, m_gLoginPassword);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        // }

        return null;

    }

    public void InitBy(File f) {

        LoadSettingsWithFile(f.getAbsolutePath());

        if (radiusServer == null) {
            try {

                // Call Web Service Operation
//                org.axiomradius.config.UserPinImplService service = new org.axiomradius.config.UserPinImplService();
//                org.axiomradius.config.UserPin port = service.getUserPinImplPort();
                radiusServer = getConfigrationSettings();
                //radiusServer = port.getRadiusServerDetails(groupId);
                if (radiusServer != null) {
                    logger.info("Sucessfully got RADIUS Server from the webservice, Group ID : " + radiusServer.getSessionId());
                } else {
                    logger.info("Error getting RADIUSSERVER");
                    return;
                }

            } catch (Exception ex) {
                logger.info("Exception while getting RADIUSSERVER" + ex.getMessage());
                return;
            }
        }

        if (_nasSettings == null) {
            _nasSettings = new HashMap<String, AxiomNasSetting>();
            List<com.mollatech.axiom.radius.AxiomRadiusClient> cl = radiusServer.getRadiusClient();
            //List<org.axiomradius.config.RadiusClient> cl = radiusServer.getRadiusClients();
            for (int i = 0; i < cl.size(); i++) {
                //org.axiomradius.config.RadiusClient rc = cl.get(i);
                AxiomRadiusClient rc = cl.get(i);
                // AxiomNasSetting obj = new AxiomNasSetting(rc.getHost(), rc.getSecretkey(), rc.getType());
                AxiomNasSetting obj = new AxiomNasSetting(rc.getRadiusClientIp(), rc.getRadiusClientSecretkey(), rc.getRadiusClientAuthtype());
                //AxiomNasSetting obj = new AxiomNasSetting("127.0.0.1", "123456", "OTPPASSWORD");
                if (!_nasSettings.containsKey(rc.getRadiusClientIp())) {
                    //logger.info("Adding client Host:" + rc.getHost() + " SecretKey:" + rc.getSecretkey() + " Type:" + rc.getType());
                    //  _nasSettings.put(rc.getHost(), obj);
                    _nasSettings.put(rc.getRadiusClientIp(), obj);
                    //_nasSettings.put("127.0.0.1", obj);
                }
            }
        }
        if (otpSetting == null) {
            //logger.info("Initializing OTP with GroupId :" + groupId);
            otpSetting = new AxiomOTPSetting(radiusServer);
        }
        if (ldapSetting == null) {
            //logger.info("Initializing LDAP: Host:" + radiusServer.getLdapHost() + " Port:" + radiusServer.getLdapPort() + " SearchPath:" + radiusServer.getLdapSearchPath());
            //ldapSetting = new AxiomLdapSetting("69.46.75.142", 389, "dc=mollatech,dc=com");
            ldapSetting = new AxiomLdapSetting(radiusServer.getLdapServerIp(), radiusServer.getLdapServerPort(), radiusServer.getLdapSearchPath());
        }
    }

    public String getClientSecret(String ip) {
        if (_nasSettings.containsKey(ip)) {
            return _nasSettings.get(ip).SecretKey;
        }
        return null;
    }

    public int getAuthType(String ip) {
        /*
         <option value="password" >Password Only</option>
         <option value="otp" >One Time Password (OTP) Only </option>
         <option value="both" >OTP And Password</option>
         <option value="followup" >Password followed by OTP</option>
         */
        if (_nasSettings.containsKey(ip)) {
            if (_nasSettings.get(ip).AuthenticationType.equals("password")) {
                return 0;
            }
            if (_nasSettings.get(ip).AuthenticationType.equals("otp")) {
                return 1;
            }
            if (_nasSettings.get(ip).AuthenticationType.equals("both")) {
                return 2;
            }
            if (_nasSettings.get(ip).AuthenticationType.equals("followup")) {
                return 3;
            }
        }

//        if (_nasSettings.containsKey(ip)) {
//            if (_nasSettings.get(ip).AuthenticationType.equals("NOOTP")) {
//                return 0;
//            }
//            if (_nasSettings.get(ip).AuthenticationType.equals("ONLYOTP")) {
//                return 1;
//            }
//            if (_nasSettings.get(ip).AuthenticationType.equals("OTPPASSWORD")) {
//                return 2;
//            }
//            if (_nasSettings.get(ip).AuthenticationType.equals("OTPCHALLENGE")) {
//                return 3;
//            }
//        }
        return -1;
    }

}
