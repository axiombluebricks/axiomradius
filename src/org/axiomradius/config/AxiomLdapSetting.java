/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.axiomradius.config;

//import com.sun.jndi.ldap.LdapCtxFactory;
import com.mollatech.axiom.radius.AxiomRadiusInterfaceImplService;
import com.mollatech.axiom.radius.AxiomRadiusStatus;
import com.mollatech.axiom.radius.Axiomradius;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.xml.namespace.QName;
import org.axiomradius.util.AllTrustManager;
import org.axiomradius.util.AllVerifier;
//import javax.naming.ldap.InitialLdapContext;

/**
 *
 * @author Push
 */
public class AxiomLdapSetting {

    private String _Server;
    private int _port;
    private String _SearchUserPath;
    //private static Log logger = LogFactory.getLog(AxiomConfig.class);

    private static AxiomRadiusInterfaceImplService m_gCoreService = null;
    private static Axiomradius m_gCorePort = null;
    private static String m_gChannelID = null;
    private static String m_gLoginID = null;
    private static String m_gLoginPassword = null;

    AxiomLdapSetting(String Server, int port, String SearchUserPath) {
        _Server = Server;
        _port = port;
        _SearchUserPath = SearchUserPath;

    }


    public boolean IsAuthenticated(String username, String pwd) {

        String wsdlname = LoadSettings.g_sSettings.getProperty("axiomradius.wsdl.name");
        m_gChannelID = LoadSettings.g_sSettings.getProperty("axiomradius.channelid");
        m_gLoginID = LoadSettings.g_sSettings.getProperty("axiomradius.remotelogin");
        m_gLoginPassword = LoadSettings.g_sSettings.getProperty("axiomradius.password");
        String ipAddress = LoadSettings.g_sSettings.getProperty("axiomradius.ipaddress");
        String strPort = LoadSettings.g_sSettings.getProperty("axiomradius.port");
        String strSecured = LoadSettings.g_sSettings.getProperty("axiomradius.secured");
        if (strSecured != null && strSecured.compareToIgnoreCase("yes") == 0) {
            SSLContext sslContext = null;
            try {
                HttpsURLConnection.setDefaultHostnameVerifier(new AllVerifier());
                try {
                    sslContext = SSLContext.getInstance("TLS");
                } catch (NoSuchAlgorithmException ex) {
                    //Logger.getLogger(NewClass.class.getName()).log(Level.SEVERE, null, ex);
                    ex.printStackTrace();
                }
                sslContext.init(null, new TrustManager[]{new AllTrustManager()}, null);
                HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

            } catch (KeyManagementException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        try { // Call Web Service Operation

            String wsdlUrl = "http://" + ipAddress + ":" + strPort + "/" + wsdlname + "/AxiomRadiusInterfaceImpl?wsdl";
            if (strSecured != null && strSecured.compareToIgnoreCase("yes") == 0) {
                wsdlUrl = "https://" + ipAddress + ":" + strPort + "/" + wsdlname + "/AxiomRadiusInterfaceImpl?wsdl";
            }

            URL url = new URL(wsdlUrl);
            QName qName = new QName("http://radius.axiom.mollatech.com/", "AxiomRadiusInterfaceImplService");

            m_gCoreService = new AxiomRadiusInterfaceImplService(url, qName);
            m_gCorePort = m_gCoreService.getAxiomRadiusInterfaceImplPort();
//            AxiomRadiusStatus arsObj = m_gCorePort.verifyRadiusCredential(
//                    m_gChannelID,
//                    m_gLoginID,
//                    m_gLoginPassword,
//                    null,null,
//                    username,
//                    "password");
             AxiomRadiusStatus arsObj = m_gCorePort.verifyRadiusCredential(
                    m_gChannelID,
                    m_gLoginID,
                    m_gLoginPassword,
                    username,
                    pwd,
                    null,
                    "password");
             if(arsObj==null){
                 return false;
             }

            if (arsObj.getErrorcode() == 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

       return false;

    }

}
