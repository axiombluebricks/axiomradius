
package org.axiomradius.config;

public class AxiomNasSetting{

    public String AuthenticationType;
    public String SecretKey;
    public String Ip;
   
    public AxiomNasSetting( String ip, String key, String authType )
    {
            AuthenticationType = authType;
            SecretKey = key;
            Ip = ip;
    }
}
