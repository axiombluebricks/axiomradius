/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.axiomradius.config;

//import axiom.core.AxiomStatus;
//import axiom.core.AxiomUser;
import com.mollatech.axiom.radius.AxiomRadiusConfiguration;
import com.mollatech.axiom.radius.AxiomRadiusInterfaceImplService;
import com.mollatech.axiom.radius.AxiomRadiusStatus;
import com.mollatech.axiom.radius.Axiomradius;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.xml.namespace.QName;
import org.axiomradius.util.AllTrustManager;
import org.axiomradius.util.AllVerifier;

/**
 *
 * @author Push
 */
public class AxiomOTPSetting {

    //private String _groupId;
    //private static Log logger = LogFactory.getLog(AxiomOTPSetting.class);
    private AxiomRadiusConfiguration radiusServer;

    private static AxiomRadiusInterfaceImplService m_gCoreService = null;
    private static Axiomradius m_gCorePort = null;
    private static String m_gChannelID = null;
    private static String m_gLoginID = null;
    private static String m_gLoginPassword = null;

    public AxiomOTPSetting(AxiomRadiusConfiguration _radiusServer) {
        radiusServer = _radiusServer;

    }
    
    
    

    public boolean validateOTP(String username, String otp) {
        AxiomRadiusStatus arsObj = verifyRadiusCredential(username, null, otp, "otp");
        if(arsObj==null){
                return false;
        }
        if (arsObj.getErrorcode() == 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean validateOTPPassword(String username, String otppassword) {
        AxiomRadiusStatus arsObj = verifyRadiusCredential(username, otppassword, null, "both");
        if(arsObj==null){
                return false;
        }
        if (arsObj.getErrorcode() == 0) {
            return true;
        } else {
            return false;
        }
    }

    private static AxiomRadiusStatus verifyRadiusCredential(
            java.lang.String username, java.lang.String password,
            java.lang.String otp, java.lang.String type) {

        //if (m_gChannelID == null) {
        String wsdlname = LoadSettings.g_sSettings.getProperty("axiomradius.wsdl.name");
        m_gChannelID = LoadSettings.g_sSettings.getProperty("axiomradius.channelid");
        m_gLoginID = LoadSettings.g_sSettings.getProperty("axiomradius.remotelogin");
        m_gLoginPassword = LoadSettings.g_sSettings.getProperty("axiomradius.password");
        String ipAddress = LoadSettings.g_sSettings.getProperty("axiomradius.ipaddress");
        String strPort = LoadSettings.g_sSettings.getProperty("axiomradius.port");
        String strSecured = LoadSettings.g_sSettings.getProperty("axiomradius.secured");
        if (strSecured != null && strSecured.compareToIgnoreCase("yes") == 0) {
            SSLContext sslContext = null;
            try {
                HttpsURLConnection.setDefaultHostnameVerifier(new AllVerifier());
                try {
                    sslContext = SSLContext.getInstance("TLS");
                } catch (NoSuchAlgorithmException ex) {
                    //Logger.getLogger(NewClass.class.getName()).log(Level.SEVERE, null, ex);
                    ex.printStackTrace();
                }
                sslContext.init(null, new TrustManager[]{new AllTrustManager()}, null);
                HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

            } catch (KeyManagementException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        //}

        try { // Call Web Service Operation

            String wsdlUrl = "http://" + ipAddress + ":" + strPort + "/" + wsdlname + "/AxiomRadiusInterfaceImpl?wsdl";
            if (strSecured != null && strSecured.compareToIgnoreCase("yes") == 0) {
                wsdlUrl = "https://" + ipAddress + ":" + strPort + "/" + wsdlname + "/AxiomRadiusInterfaceImpl?wsdl";
            }

            URL url = new URL(wsdlUrl);
            QName qName = new QName("http://radius.axiom.mollatech.com/", "AxiomRadiusInterfaceImplService");

            m_gCoreService = new AxiomRadiusInterfaceImplService(url, qName);
            m_gCorePort = m_gCoreService.getAxiomRadiusInterfaceImplPort();
            AxiomRadiusStatus arsObj =m_gCorePort.verifyRadiusCredential(
                    m_gChannelID,
                    m_gLoginID,
                    m_gLoginPassword,
                    username,
                    password,
                    otp,
                    type);
            
            
            return arsObj;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
        //return port.verifyRadiusCredential(channelid, loginid, loginpassword, username, password, otp, type);
    }

   
    
    
    
}
