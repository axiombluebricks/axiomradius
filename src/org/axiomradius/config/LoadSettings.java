package org.axiomradius.config;

import java.util.Date;
import java.util.Properties;

public class LoadSettings {

    public static Properties g_sSettings = null;
    public static String g_strPath = null;

    public static void LoadSettingsWithFile(String filepath) {
        try {
            PropsFileUtil p = new PropsFileUtil();
            if (p.LoadFile(filepath) == true) {
                g_sSettings = p.properties;
            } else {
                System.out.println((new Date()) + ">> Axiom Radius Configuration File is missing >> " + filepath);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

//    static {
//        String sep = System.getProperty("file.separator");
//        String usrhome = System.getProperty("catalina.home");
//        usrhome += sep + "axiomv2-settings";
//        g_strPath = usrhome + sep;
//        String filepath = usrhome + sep + "axiomradius.conf";
//        PropsFileUtil p = new PropsFileUtil();
//
//        if (p.LoadFile(filepath) == true) {
//            g_sSettings = p.properties;
////            System.out.println("dbsetting setting file loaded >>" + filepath);
//
//        } else {
//            System.out.println((new Date()) + ">> Axiom Radius Configuration File is missing >> " + filepath);
//        }
//    }
//    public static void LoadManually(String path) {
//        DBSettingManual(path);
//      
//    }
//
//    private static void DBSettingManual(String path) {
//        String sep = System.getProperty("file.separator");
//        String usrhome = path;
//        usrhome += sep + "axiomv2-settings";
//        g_strPath = usrhome + sep;
//        String filepath = usrhome + sep + "axiomradius.conf";
//        PropsFileUtil p = new PropsFileUtil();
//
//        if (p.LoadFile(filepath) == true) {
//            g_sSettings = p.properties;
////            System.out.println("manually loaded dbsetting setting file loaded >>" + filepath);
//
//        } else {
//            System.out.println("manually loaded dbsetting setting file failed to load >> " + filepath);
//        }
//    }
//
//   
}
